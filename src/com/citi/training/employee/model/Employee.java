package com.citi.training.employee.model;

import java.util.Date;

import java.util.concurrent.atomic.AtomicInteger;

public class Employee 
{
    // an integer that is incremented with each new employee
    private static AtomicInteger idGenerator = new AtomicInteger(0);

    private int id;
    private String name;
    private double salary;
    private Date joined = new Date();

    // Constructors. Note, no default constructor.
    public Employee(String name) 
    {
        this(name, 7000); // Use constructor chaining.
    }

    public Employee(String name, double salary) 
    {
        this.id = idGenerator.getAndAdd(1);
        this.name = name;
        this.salary = salary;
    }

    public int getId() 
    {
        return id;
    }

    public String getName() 
    {
        return name;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public double getSalary() 
    {
        return salary;
    }

    public void setSalary(double salary) 
    {
        this.salary = salary;
    }

    public Date getJoined() 
    {
        return joined;
    }

    public void setJoined(Date joined) 
    {
        this.joined = joined;
    }

    // Business methods.
    public void payRaise(double amount) 
    {
        salary += amount;
    }

    public void payBonus() 
    {
        // Give employee a 1% bonus.
        salary *= 1.01;
    }

    public void payBonus(double percentBonus) 
    {
        // Give employee a bonus as specified by parameter.
        salary *= 1 + percentBonus / 100;
    }

    @Override
    public String toString() 
    {
        return "Employee [id=" + id + ", name=" + name + ", salary=" + salary +
               ", joined=" + joined + "]";
    }
}